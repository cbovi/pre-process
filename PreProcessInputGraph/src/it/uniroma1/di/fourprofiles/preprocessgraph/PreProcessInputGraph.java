package it.uniroma1.di.fourprofiles.preprocessgraph;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;
import java.util.regex.Pattern;

import it.uniroma1.di.fourprofiles.preprocessgraph.util.EasyReader;
import it.uniroma1.di.fourprofiles.preprocessgraph.util.EasyWriter;

/*
1. Map each node id to a contiguous number in 0, … , (number_unique_ids - 1)
2. Replace each id with its new, contiguous node id
3. For each line (edge i,j) i\tj :  If i < j, replace i\tj with j\ti
4. Get unique lines
 */

public class PreProcessInputGraph {
	
	private static final String SPACE = " ";
	private static final String TAB = "\t";
	private static final Pattern SEPARATOR = Pattern.compile("[\t ]");
	private static final long MB = 1024;
	private static final String MBs = " MBs";
	
	public static void main(String[] args) {
		
		logMem();

		String inputFileName = args[0];
		String preProcessSuffix = args[1];
		
		HashSet<String> replacedLines = replaceTokenWithIntId(inputFileName);
		
		deleteDoubleEdges(inputFileName, preProcessSuffix, replacedLines);
		
	}



	private static HashSet<String> replaceTokenWithIntId(String inputFileName) {
		
		
	    //https://courses.cs.washington.edu/courses/cse326/02au/assignments/EasyReader.java
		//EasyReader console = new EasyReader();
		//System.out.print("Enter input file name: ");
		//String inputFileName = console.readLine();		

		EasyReader inputFile = new EasyReader(inputFileName);
		if (inputFile.bad()) {
			 System.err.println("Can't open " + inputFile);
			 System.exit(1);
		}
	
		
		HashMap<String, String> temp = new HashMap<String, String>();
		HashSet<String> replacedLines = new HashSet<String>();
		Integer count=0;		
		
		
//primo ciclo while per
//- la lettura del file di input 
//- per effettuare il replace dei token con id interi		
		
		while (!inputFile.eof()) {
			 String line = inputFile.readLine();
			 if (line != null && !line.startsWith("#")) {
				 
				 line = check_TAB_SPACE(line);
				 
				 if (!line.isEmpty()) {
					 
					 
						String[] tokens = SEPARATOR.split(line.toString());
						String source=tokens[0];
						String target=tokens[1];
						if (temp.get(source)==null) {
							temp.put(source, count.toString());
							count++;
						}
						if (temp.get(target)==null) {
							temp.put(target, count.toString());
							count++;
						}
						
						String first=temp.get(source);
						String second=temp.get(target);
						replacedLines.add(first + TAB + second);					 

				 }
				 
			 }     
		}
		
		inputFile.close();
		return replacedLines;
	}


	/**
	 * Returns a negative integer, zero, or a positive integer as this tsv1 parameter is 
	 * less than, equal to, or greater than the tsv2 parameter
	 *
	 */
	static class TSVComparator implements Comparator<String> {
		
	    @Override
	    public int compare(String tsv1, String tsv2) {

			String[] tokens1 = SEPARATOR.split(tsv1.toString());
			Integer source1 = Integer.valueOf(tokens1[0]);
			Integer target1 = Integer.valueOf(tokens1[1]);
			
			String[] tokens2 = SEPARATOR.split(tsv2.toString());
			Integer source2 = Integer.valueOf(tokens2[0]);
			Integer target2 = Integer.valueOf(tokens2[1]);
			
			int compare1 = source1.compareTo(source2);
			Integer result = null;
			if (compare1 == 0) {
				int compare2 = target1.compareTo(target2);
				result = compare2;
			}
			else {
				result = compare1;
			}
	    	
	        return result;
	    }
	}
	
	
	private static void deleteDoubleEdges(
			String inputFileName, 
			String preProcessSuffix,
			HashSet<String> replacedLines) {
		
		
		long inputFileEdgesCount = 0;
		long inputFileDoubleEdgesCount = 0;
		long cappi = 0;
		TreeSet<String> uniqueLines = new TreeSet<String>(new TSVComparator());
		HashSet<String> inputVertexes = new HashSet<String>();
		
		
		Iterator<String> iter = replacedLines.iterator();
//Secondo ciclo while su tutto il contenuto dell'hashset replacedLines 
//per l'eliminazione degli archi diretti doppi
		while (iter.hasNext()) {
			
			String line =iter.next();
				
			inputFileEdgesCount++;
			 
			String[] tokens = SEPARATOR.split(line.toString());
			long first = Long.parseLong(tokens[0]);
			long second = Long.parseLong(tokens[1]);
			 
			inputVertexes.add(tokens[0]);
			inputVertexes.add(tokens[1]);
			 
			if (first==second) { cappi++;
			                     continue;
			 
			}
			 
			if (first < second) {
				line = second + TAB + first;
			}
			 
			if (uniqueLines.contains(line)) {
				inputFileDoubleEdgesCount++;
			}
			else {
				uniqueLines.add(line);
			}			
			
			
		}
		
		
		
			
		
		writeOutputFiles(
				inputFileName, 
				preProcessSuffix, 
				inputFileEdgesCount, 
				inputFileDoubleEdgesCount, 
				cappi,
				inputVertexes, 
				uniqueLines);
		
	}

	
	
	private static void writeOutputFiles(
			String inputFileName, 
			String preProcessSuffix, 
			long inputFileEdgesCount,
			long inputFileDoubleEdgesCount, 
			long cappi, 
			HashSet<String> inputVertexes,
			TreeSet<String> uniqueLines) {
		
		long outputFileEdgesCount;
		outputFileEdgesCount = uniqueLines.size();
		
		HashSet<String> outputVertexes = new HashSet<String>();
		HashMap<String,Integer> edgeCounts = new HashMap<String,Integer>();
		Integer max = 0;
		
		
		
		EasyWriter outputFile = new EasyWriter(/*"../"+*/inputFileName+preProcessSuffix);
		Iterator<String> it = uniqueLines.iterator();
//Terzo ciclo while su tutto il contenuto dell'hashset per scriverlo sul file di output
		while (it.hasNext()) {
			String outputLine =it.next();
			String[] tokens = SEPARATOR.split(outputLine.toString());
			String source = tokens[0];
			String target = tokens[1];
			outputVertexes.add(source);
			outputVertexes.add(target);
			outputFile.println(outputLine);
			
			
			Integer count = edgeCounts.get(source);
			if (count == null) {
				edgeCounts.put(source, 1);
			}
			else {
				count++;
				if (max<count)
					max=count;
				edgeCounts.put(source, count);
			}
			
			
			count = edgeCounts.get(target);
			if (count == null) {
				edgeCounts.put(target, 1);
			}
			else {
				count++;
				if (max<count)
					max=count;
				edgeCounts.put(target, count);
			}
			
			
			
		}
		
		outputFile.close();
		
		
		long sum = 0;
		Iterator<String> ecIt = edgeCounts.keySet().iterator();
		while (ecIt.hasNext()) {
			String vertex = ecIt.next();
			sum += edgeCounts.get(vertex);
		}
		
		double averageOfEdges = (double)sum / (double)edgeCounts.size();
		
		BigInteger UNO = BigInteger.valueOf(1);
		BigInteger DUE = BigInteger.valueOf(2);
		BigInteger TRE = BigInteger.valueOf(3);
		BigInteger VENTIQUATTRO = BigInteger.valueOf(24);

		BigDecimal UNODECIMAL = new BigDecimal(1);
		BigDecimal DUEDECIMAL = new BigDecimal(2);
		BigDecimal vertexesDecimal = new BigDecimal(outputVertexes.size());
		
		BigInteger vertexes = BigInteger.valueOf(outputVertexes.size());
		BigDecimal outputEdge = new BigDecimal(outputFileEdgesCount);
		
		//preso dal link https://stackoverflow.com/questions/4591206/arithmeticexception-non-terminating-decimal-expansion-no-exact-representable
		//https://jaydeepm.wordpress.com/2009/06/04/bigdecimal-and-non-terminating-decimal-expansion-error/
		BigInteger totalSumOf4Profiles = ((vertexes.multiply(vertexes.subtract(UNO)).multiply(vertexes.subtract(DUE)).
				                   multiply(vertexes.subtract(TRE))).divide(VENTIQUATTRO));
		
		// pag. 14 of http://www.uniroma2.it/didattica/grfbis/deposito/Dispense_Grafi_e_reti_di_flusso_-_Antonio_Iovanella_AA07_08.pdf
		BigDecimal density = (outputEdge.multiply(DUEDECIMAL)).divide( 
				((vertexesDecimal.multiply(vertexesDecimal.subtract(UNODECIMAL)))),8,RoundingMode.HALF_EVEN);
		
		
//Scrittura del file di log		
		EasyWriter logFile = new EasyWriter(inputFileName+preProcessSuffix+"-log");
		logFile.println("COUNTS:");
		logFile.println("input file vertexes count = "+inputVertexes.size());
		logFile.println("output file vertexes count = "+outputVertexes.size());
		logFile.println("input file edges count = "+inputFileEdgesCount);
		logFile.println("output file edges count = "+outputFileEdgesCount);
		logFile.println("input file double edges count = "+inputFileDoubleEdgesCount);
		logFile.println("input file cappi = "+cappi);
		logFile.println("average number of edges per vertex = "+sum+" / "+edgeCounts.size()+" = "+averageOfEdges);
		logFile.println("max number of edges per vertex = "+max);
		logFile.println("density = "+density);
		logFile.println("totalSumOf4Profiles = "+totalSumOf4Profiles);
		
		logFile.close();
		
		
		System.out.println("\n\n\nCOUNTS:");
		System.out.println("input file vertexes count = "+inputVertexes.size());
		System.out.println("output file vertexes count = "+outputVertexes.size());
		System.out.println("input file edges count = "+inputFileEdgesCount);
		System.out.println("output file edges count = "+outputFileEdgesCount);
		System.out.println("input file double edges count = "+inputFileDoubleEdgesCount);
		System.out.println("input file cappi = "+cappi);
		System.out.println("average number of edges per vertex = "+sum+" / "+edgeCounts.size()+" = "+averageOfEdges);
		System.out.println("max number of edges per vertex = "+max);
		System.out.println("density = "+density);
		System.out.println("binomial coefficient = "+totalSumOf4Profiles);
		
		Map<String,Integer> sortedEdgeCounts = sortByValue(edgeCounts);
		EasyWriter edgesCountFile = new EasyWriter(inputFileName+preProcessSuffix+"-edgesCount");
		Iterator<String> it2 = sortedEdgeCounts.keySet().iterator();
		while (it2.hasNext()) {
			String vertex = it2.next();
			edgesCountFile.println(vertex+"\t"+sortedEdgeCounts.get(vertex));
		}
		
		edgesCountFile.close();
	}
	
	
	// https://stackoverflow.com/questions/109383/sort-a-mapkey-value-by-values
    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Entry<K, V>> list = new ArrayList<>(map.entrySet());
        
        list.sort(Entry.comparingByValue());
        
        Collections.reverse(list);

        Map<K, V> result = new LinkedHashMap<>();
        for (Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

	
	private static void logMem() {
		// https://stackoverflow.com/questions/3571203/what-are-runtime-getruntime-totalmemory-and-freememory
		System.out.println("\n\n\nMEMORY:");
		System.out.println("total memory (-Xmx) = "+Runtime.getRuntime().totalMemory()/MB + MBs);
		System.out.println("max memory = "+Runtime.getRuntime().maxMemory()/MB + MBs);
		System.out.println("free memory = "+Runtime.getRuntime().freeMemory()/MB + MBs + "   (Current allocated free memory, is the current allocated space ready for new objects.)");
		long usedMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println("used memory = "+usedMemory/MB + MBs);
		long totalFreeMemory = Runtime.getRuntime().maxMemory() - usedMemory;
		System.out.println("total free memory = "+totalFreeMemory/MB + MBs);
	}

	
	/**
	 * Verifies is a line of input file contains SPACE instead of TAB
	 */
	private static String check_TAB_SPACE(String line) {
		
		int lineLengthBeforeTrim = line.length();
		String lineBeforeTrim = line;
		line = line.trim();
		int lineLengthAfterTrim = line.length();
		
		if (lineLengthBeforeTrim != lineLengthAfterTrim) {
			System.out.println("line before trim=\""+lineBeforeTrim+"\"");
			System.out.println("line after trim=\""+line+"\"");
		}
		 
		if (line.indexOf(TAB) == -1) {
			System.out.println("line without TAB=\""+line+"\"");
		}
		
		// verifico se è stato usato per errore uno space al posto del tab il che significherebbe
		// che l'input non è un tsv (tab separated value)
		if (line.indexOf(SPACE) != -1) {
			System.out.println("line with empty space=\""+line+"\"");
		}
		
		return line;
	}
	
	
}
