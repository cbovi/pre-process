package it.uniroma1.di.fourprofiles.preprocessgraph.util;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class U {
	
	
	
	public static final String E = "";
	public static final String C = ", ";
	public static final String N = "\n";
	public static final String N2 = N+N;
	public static final String N4 = N2+N2;
	public static final String S = " <-----------------------------";
	public static final String SN = S+N;
	public static final String SN2 = S+N2;
	public static final String SN4 = S+N4;
	public static final String S2 = " <<<<<<<<<<<<<<";
	public static final String S2N = S2+N;
	
	

	public static String stackTraceToString(Throwable t)
	  {
	    ByteArrayOutputStream b = new ByteArrayOutputStream();
	    PrintStream p = new PrintStream(b);
	    t.printStackTrace(p);
	    p.flush();
	    return b.toString();
	  }

	public static String shortStackTraceToString(Throwable t)
	  {
	    StackTraceElement[] stackTrace = t.getStackTrace();
	    StringBuilder logMsg = new StringBuilder();
	    
	    int i = 1;
	    for (StackTraceElement s : stackTrace) {
	      i++;
	      logMsg.append(s.toString());
	      logMsg.append(" ");
	      if (i == 6)
	        break;
	    }
	    return logMsg.toString();
	  }

	public static String stackTraceToString()
	  {
	    StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
	    StringBuilder logMsg = new StringBuilder();
	    
	    for (StackTraceElement s : stackTrace) {
	      logMsg.append(s.toString());
	      logMsg.append("\n");
	    }
	    
	    return logMsg.toString();
	  }

	public static String shortStackTraceToString()
	  {
	    StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
	    StringBuilder logMsg = new StringBuilder();
	    
	    int i = 1;
	    for (StackTraceElement s : stackTrace) {
	      i++;
	      logMsg.append(s.toString());
	      logMsg.append(" ");
	      if (i == 6)
	        break;
	    }
	    return logMsg.toString();
	  }





}
